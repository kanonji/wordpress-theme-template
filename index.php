<!DOCTYPE html>
<!--[if lt IE 7 ]><html <?php language_attributes(); ?> class="no-js ie6"><![endif]-->
<!--[if (gt IE 6)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<title><?php bloginfo( 'name' ); ?><?php wp_title( '&mdash;' ); ?></title>
<link rel="stylesheet" href="//libcache.herokuapp.com/css/normalize.css/normalize.css">
<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/asset/css/clearfix.css">
<script src="//libcache.herokuapp.com/modernizr/2.6.2/modernizr.mini.js"></script>
<link rel="shortcut icon" href="/img/favicon.ico">
<?php if ( is_singular() && get_option( 'thread_comments') ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="container">
  <header id="header">
    <h1><a href="<?php bloginfo('url'); ?>">
        <?php if(current_theme_supports('custom-header') && get_header_image()) : ?>
        <img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="<?php bloginfo('name'); ?>">
        <?php else : ?>
        <a href="<?php bloginfo('url'); ?>"><?php bloginfo( 'name' ); ?></a>
        <?php endif; ?>
    </h1></a>
    <p id="description"><?php bloginfo( 'description' ); ?></p>
    <?php if ( has_nav_menu( 'menu' ) ) : wp_nav_menu(); else : ?>
    <ul><?php wp_list_pages( 'title_li=&depth=-1' ); ?></ul>
    <?php endif; ?>
  </header>
  <div class="wrapper">
    <div id="primary">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <article <?php post_class(); ?>>
        <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
        <?php the_content(); ?>
        <?php if ( !is_singular() && get_the_title() == '' ) : ?>
        <a href="<?php the_permalink(); ?>">(more...)</a>
        <?php endif; ?>
        <?php if ( is_singular() ) : ?>
        <div class="pagination"><?php wp_link_pages(); ?></div>
        <?php endif; ?>
        <div class="clear"> </div>
      </article>
      <?php if ( is_singular() ) : ?>
      <aside class="meta">
        <p>Posted by <?php the_author_posts_link(); ?>
        on <a href="<?php the_permalink(); ?>"><?php the_date(); ?></a>
        in <?php the_category( ', ' ); ?><?php the_tags( ', ' ); ?></p>
      </aside><!-- meta -->
      <?php comments_template(); ?>
      <?php endif; ?>
      <?php endwhile; else: ?>
      <article class="hentry"><h1><?php _e('Sorry, the page you requested cannot be found', 'domain'); ?></h1></article>
      <?php endif; ?>
      <?php if ( is_singular() || is_404() ) : ?>
      <div class="left"><a href="<?php bloginfo( 'url' ); ?>">&laquo; Home page</a></div>
      <?php else : ?>
      <div class="left"><?php next_posts_link( '&laquo; Older posts' ); ?></div>
      <div class="right"><?php previous_posts_link( 'Newer posts &raquo;' ); ?></div>
      <?php endif; ?>
    </div><!-- primary -->
    <?php if ( is_active_sidebar( 'widgets' ) ) : ?>
    <div id="secondary" class="widgets"><?php dynamic_sidebar( 'widgets' ); ?></div>
    <?php endif; ?>
  </div><!-- wrapper -->
  <footer id="footer">
    <small id="copyright">Copyright &copy;. All rights reserved.</small>
  </footer>
</div><!-- container -->
<?php wp_footer(); ?>
</body>
</html>
<!-- vim: set ts=2 sw=2 sts=2 et ff=unix ft=html : -->
