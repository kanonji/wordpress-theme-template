<?php
//To debug action/filter comment out this.
//var_dump($GLOBALS['wp_filter']);
//If defined it is used for large size of images.
if (!isset($content_width)) $content_width = 768;
//Enable Custom Menu and register. call `add_theme_support('menu')` implicitly.
if (function_exists('register_nav_menu')) register_nav_menu( 'menu', __('Menu', 'domain') );
if (function_exists('add_theme_support')) {
    //Publish feed urls for post and comment. <link rel="alternate" type="application/rss+xml". From 3.0.0
    add_theme_support('automatic-feed-links');
    //Enable Eye-catch images. From 2.9.0
    add_theme_support('post-thumbnails');
    if(version_compare($wp_version, '3.4', '>=')) add_theme_support('custom-header', array('header-text' => false)); else add_custom_image_header();
    if(version_compare($wp_version, '3.4', '>=')) add_theme_support('custom-background'); else add_custom_background();
}
if ( function_exists('register_sidebar') ) register_sidebar( array(
    'name' => __( 'Widgets', 'domain' ),
    'id' => 'widgets',
    'before_widget' => '<aside class="widget %2$s">',
    'after_widget' => '</aside><!-- widget -->',
    'before_title' => '<h1>',
    'after_title' => '</h1>') );
?>
